# 基于ESP8266+BY8301语音模块的与山地车捉迷藏的小项目

## 写在前面：

> 不知道大家有没有经历过，在茫茫车海中找不到自己山地车情况，针对这个痛点（开玩笑的），我做了个小玩意，纯属娱乐，拿出来，在疫情逐渐加剧的时候，给大家带来一份欢乐。

B站有项目的概要视频，不喜欢文字的同学可以去看看，看完别忘了点赞哦！

[视频网址]: https://www.bilibili.com/video/BV1eS4y13724?share_source=copy_web

项目所有软件与硬件支持都在我的码云仓库里。

[码云仓库]: https://gitee.com/iron2222/esp8266--by9301-small-project.git

下面就是这个项目的简单介绍了。

## 硬件支持：

### 电路元件

ESP8266模块、BY8301语音控制芯片、flash芯片、功放芯片、扬声器、电容、电阻、二极管、LED。

### 原理图

<img src="https://api2.mubu.com/v3/document_image/cd39e08e-716c-4680-a76e-5d70d9fc76d6-13059727.jpg" style="zoom:40%;" />

原理部分比较简单，就是连一连控制的IO引脚，别的好像也没什么要注意的。

### PCB

<img src="https://api2.mubu.com/v3/document_image/5747de46-2c51-4074-85ba-82bb0441c31f-13059727.jpg" style="zoom:40%;" />

大家想复现的时候，我建议把电阻、电容的封装换成最大号的，我第一次没经验，瞎弄，焊接的时候可是废了老鼻子劲了。

真的是吃一堑长一智。

### 打样

嘉立创，免费打样，一个月一个用户有两次机会。

要求10cm×10cm以内，二层板，四层板，都可以，颜色可任选，我选了黑色（因为酷），因此我等了差不多有两周时间（绿色的最快），还碰到嘉立创服务器崩溃，维护了两天。

<img src="https://api2.mubu.com/v3/document_image/5e301d9a-861b-4e3d-8467-ef26a6867586-13059727.jpg" style="zoom:50%;" />

### 焊接完成

<img src="https://api2.mubu.com/v3/document_image/13c07014-a905-47ec-81d1-7059057c2173-13059727.jpg" style="zoom:50%;" />

看着还可以，但这已经不知道是焊接的第几个板子了，中间焊不好，老是把焊盘给带下来，最后直接用0欧姆的电阻当飞线给连起来了，再次重申：在空间允许的情况下，一定要选大封装，要分散开布置，我的血泪教训！！

接下来就是软件部分了。

## 软件支持：

### 开发平台

Arduino平台，大量的集成库，贼方便~

<img src="https://api2.mubu.com/v3/document_image/f4585c9c-0381-497a-ba2c-51af81cb58e6-13059727.jpg" style="zoom:50%;" />

### 逻辑控制部分代码

代码分为两部分，一部分烧录直接烧录到ESP8266，用来构建个网络服务器端。

```c

#include <ESP8266WiFi.h>      // 本程序使用ESP8266WiFi库
#include <ESP8266WiFiMulti.h> // 本程序使用ESP8266WiFiMulti库
#include <ESP8266WebServer.h> // 本程序使用ESP8266WebServer库
#include <FS.h>               // 本程序使用SPIFFS库

ESP8266WiFiMulti wifiMulti;     // 建立ESP8266WiFiMulti对象,对象名称是'wifiMulti'
 
ESP8266WebServer esp8266_server(80);    // 建立网络服务器对象，该对象用于响应HTTP请求。监听端口（80）

int button1Pin = D1; 
int button2Pin = D2;
int button3Pin = D3;
int button4Pin = D4;
int button5Pin = D5;
int button6Pin = D6;
                                   
void setup(){
  Serial.begin(9600);        
  Serial.println("");
  
  pinMode(LED_BUILTIN, OUTPUT);      // 初始化NodeMCU控制板载LED引脚为OUTPUT
  pinMode(button1Pin, OUTPUT);
  pinMode(button2Pin, OUTPUT);
  pinMode(button3Pin, OUTPUT);
  pinMode(button4Pin, OUTPUT);
  pinMode(button5Pin, OUTPUT);
  pinMode(button6Pin, OUTPUT);
  
  digitalWrite(button1Pin,HIGH);
  digitalWrite(button2Pin,HIGH);
  digitalWrite(button3Pin,HIGH);
  digitalWrite(button4Pin,HIGH);
  digitalWrite(button5Pin,HIGH);
  digitalWrite(button6Pin,HIGH);
  
  //通过addAp函数存储  WiFi名称       WiFi密码
  wifiMulti.addAP("iron2222", "184******009"); // 将需要连接的一系列WiFi ID和密码输入这里
  wifiMulti.addAP("ssid_from_AP_2", "your_password_for_AP_2"); // ESP8266-NodeMCU再启动后会扫描当前网络
  wifiMulti.addAP("ssid_from_AP_3", "your_password_for_AP_3"); // 环境查找是否有这里列出的WiFi ID。如果有
  Serial.println("Connecting ...");                            // 则尝试使用此处存储的密码进行连接。
 
  int i = 0;                                 
  while (wifiMulti.run() != WL_CONNECTED) {  // 在当前环境中搜索addAP函数所存储的WiFi
    delay(1000);                             // 如果搜到多个存储的WiFi那么NodeMCU
    Serial.print(i++); Serial.print('.');                       // 将会连接信号最强的那一个WiFi信号。
  }                                          
                                         
  // WiFi连接成功后将通过串口监视器输出连接成功信息 
  Serial.println('\n');                     // WiFi连接成功后
  Serial.print("Connected to ");            // NodeMCU将通过串口监视器输出。
  Serial.println(WiFi.SSID());              // 连接的WiFI名称
  Serial.print("IP address:\t");            // 以及
  Serial.println(WiFi.localIP());           // NodeMCU的IP地址

  if(SPIFFS.begin()){                       // 启动闪存文件系统
    Serial.println("SPIFFS Started.");
  } else {
    Serial.println("SPIFFS Failed to Start.");
  }
  
  esp8266_server.on("/1-Control", handle1Control); // 告知系统如何处理/1-Control请求
  esp8266_server.on("/2-Control", handle2Control);
  esp8266_server.on("/3-Control", handle3Control);
  esp8266_server.on("/4-Control", handle4Control);
  esp8266_server.on("/5-Control", handle5Control);
  esp8266_server.on("/6-Control", handle6Control);
       
  esp8266_server.onNotFound(handleUserRequest);        // 告知系统如何处理其它用户请求     
  
  esp8266_server.begin();                   // 启动网站服务                                  
  Serial.println("HTTP server started");    
}

void loop(){
  esp8266_server.handleClient();  //处理用户请求
  digitalWrite(LED_BUILTIN,HIGH);
}                                

// 处理/1-Control请求  
void handle1Control(){
   digitalWrite(button1Pin,!digitalRead(button1Pin));
   delay(500);
   digitalWrite(button1Pin,HIGH);
 
   esp8266_server.sendHeader("Location", "/yuyinctrl.html");       
   esp8266_server.send(303);  
}
void handle2Control(){
   digitalWrite(button2Pin,!digitalRead(button2Pin));
   delay(500);
   digitalWrite(button2Pin,HIGH);
 
   esp8266_server.sendHeader("Location", "/yuyinctrl.html");       
   esp8266_server.send(303);  
}
void handle3Control(){
   digitalWrite(button3Pin,!digitalRead(button3Pin));
   delay(500);
   digitalWrite(button3Pin,HIGH);
 
   esp8266_server.sendHeader("Location", "/yuyinctrl.html");       
   esp8266_server.send(303);  
}
void handle4Control(){
   digitalWrite(button4Pin,!digitalRead(button4Pin));
   delay(500);
   digitalWrite(button4Pin,HIGH);
 
   esp8266_server.sendHeader("Location", "/yuyinctrl.html");       
   esp8266_server.send(303);  
}
void handle5Control(){
   digitalWrite(button5Pin,!digitalRead(button5Pin));
   delay(500);
   digitalWrite(button5Pin,HIGH);
 
   esp8266_server.sendHeader("Location", "/yuyinctrl.html");       
   esp8266_server.send(303);  
}
void handle6Control(){
   digitalWrite(button6Pin,!digitalRead(button6Pin));
   delay(500);
   digitalWrite(button6Pin,HIGH);
 
   esp8266_server.sendHeader("Location", "/yuyinctrl.html");       
   esp8266_server.send(303);  
}

                                                                     
// 处理用户浏览器的HTTP访问
void handleUserRequest() {         
     
  // 获取用户请求资源(Request Resource）
  String reqResource = esp8266_server.uri();
  Serial.print("reqResource: ");
  Serial.println(reqResource);
  
  // 通过handleFileRead函数处处理用户请求资源
  bool fileReadOK = handleFileRead(reqResource);

  // 如果在SPIFFS无法找到用户访问的资源，则回复404 (Not Found)
  if (!fileReadOK){                                                 
    esp8266_server.send(404, "text/plain", "404 Not Found"); 
  }
}

bool handleFileRead(String resource) {            //处理浏览器HTTP访问

  if (resource.endsWith("/")) {                   // 如果访问地址以"/"为结尾
    resource = "/index.html";                     // 则将访问地址修改为/index.html便于SPIFFS访问
  } 
  
  String contentType = getContentType(resource);  // 获取文件类型
  
  if (SPIFFS.exists(resource)) {                     // 如果访问的文件可以在SPIFFS中找到
    File file = SPIFFS.open(resource, "r");          // 则尝试打开该文件
    esp8266_server.streamFile(file, contentType);// 并且将该文件返回给浏览器
    file.close();                                // 并且关闭文件
    return true;                                 // 返回true
  }
  return false;                                  // 如果文件未找到，则返回false
}

// 获取文件类型
String getContentType(String filename){
  if(filename.endsWith(".htm")) return "text/html";
  else if(filename.endsWith(".html")) return "text/html";
  else if(filename.endsWith(".css")) return "text/css";
  else if(filename.endsWith(".js")) return "application/javascript";
  else if(filename.endsWith(".png")) return "image/png";
  else if(filename.endsWith(".gif")) return "image/gif";
  else if(filename.endsWith(".jpg")) return "image/jpeg";
  else if(filename.endsWith(".ico")) return "image/x-icon";
  else if(filename.endsWith(".xml")) return "text/xml";
  else if(filename.endsWith(".pdf")) return "application/x-pdf";
  else if(filename.endsWith(".zip")) return "application/x-zip";
  else if(filename.endsWith(".gz")) return "application/x-gzip";
  return "text/plain";
}
```

### 网页构建代码

#### 主页面

```html
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="UTF-8">
	<title>语音模块控制</title>
</head>
<body>
	<center>
	<a href="https://gitee.com/iron2222/esp8266" target="_blank"><img src="/img/lalala.png" alt="iron2222"></a>
	<h1>欢迎来到我的小家！</h1>
	<p><a href="yuyinctrl.html">前往语音控制页面</a></p>
	<p>此页面用于演示如何通过网页按钮来控制语音播放。</p>
    <p>详情请参考我的码云仓库： <a href="https://gitee.com/iron2222/esp8266" target="_blank">https://gitee.com/iron2222/esp8266</a> 
	</center>
</body>

</html>
```

#### 语音控制页面

```html
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="UTF-8">
	<title>语音模块控制</title>
</head> 
<body>
	<center>
	<h1>语音模块选择</h1>
	<p>通过以下按键，您可以选择所需要播放的语音</p>
   <form action="1-Control"><input type="submit" value="语音1"style="width:100px;height:60px">
   </form>
   <br>
   <form action="2-Control"><input type="submit" value="语音2"style="width:100px;height:60px">
   </form>
   <br>
   <form action="3-Control"><input type="submit" value="语音3"style="width:100px;height:60px">
   </form>
   <br>
   <form action="4-Control"><input type="submit" value="语音4"style="width:100px;height:60px">
   </form>
   <br>
   <form action="5-Control"><input type="submit" value="语音5"style="width:100px;height:60px">
   </form>
   <br>
   <form action="6-Control"><input type="submit" value="语音6"style="width:100px;height:60px">
   </form>
   <br>
   <form action="index.html"><input type="submit" value="返回首页"style="width:100px;height:60px">
   </form>
   	<p>此页面用于演示如何通过网页按钮来控制语音播放。</p>
    <p>详情请参考我的码云网址： <a href="https://gitee.com/iron2222/esp8266" target="_blank">https://gitee.com/iron2222/esp8266</a> 
   </center>
</body>

</html>
```

#### 网页效果展示

<img src="https://api2.mubu.com/v3/document_image/6c7ffcda-a6b8-438f-b0a0-2725dab9d62f-13059727.jpg" style="zoom:50%;" />

<img src="https://api2.mubu.com/v3/document_image/3605f6c9-06b0-4a9d-a012-fdc3cc74c192-13059727.jpg" style="zoom:50%;" />

网页文件和图片资源是与要通过，ardunio直接上传到你的ESP8266这个板子里面的。具体方式大家可以参考太极创客网站，里面的教程说的相当清楚。附上地址吧：

[太极创客]: http://www.taichi-maker.com/

<img src="https://api2.mubu.com/v3/document_image/e98ee0b3-315e-4db1-a3ae-951b73e366c8-13059727.jpg" style="zoom:50%;" />

以上便是软件的所有内容。

## 实际效果演示：

演示效果大家可以去看我那个视频，因为使用的是wifi连接，所以不建议室内，室外50~100m还是可以连接上的，但可能你得换一个功率相当大的喇叭，才能听得到你的山地车的回应。

<img src="https://api2.mubu.com/v3/document_image/85b5f44c-a177-4cd2-9f3b-0d8288749530-13059727.jpg" style="zoom:50%;" />

## 总结

第一次从头到尾做一个小东西，收获很多，教训也很多，加油~

祝大家早安，午安和晚安！！！